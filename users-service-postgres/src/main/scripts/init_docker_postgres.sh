#!/bin/bash

DATABASE_NAME="users"
DB_DUMP_LOCATION="/tmp/psql_data/structure.sql"

echo "*** CREATING DATABASE ***"

psql postgres <<EOSQL
  CREATE DATABASE "$DATABASE_NAME";
  GRANT ALL PRIVILEGES ON DATABASE "$DATABASE_NAME" TO postgres;
EOSQL

psql postgres -d "$DATABASE_NAME" < "$DB_DUMP_LOCATION";

echo "*** DATABASE CREATED! ***"
