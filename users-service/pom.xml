<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.rogelioorts.assignments.redhat</groupId>
    <artifactId>users-service</artifactId>
    <version>1.0.0-SNAPSHOT</version>

    <properties>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>

        <vertx.version>3.7.0</vertx.version>
        <swagger.version>1.5.22</swagger.version>
        <sl4j.version>1.7.6</sl4j.version>
        <jose4j.version>0.5.0</jose4j.version>
        <scala.version>2.12.7</scala.version>

        <maven.compiler.plugin.version>3.8.0</maven.compiler.plugin.version>
        <build-helper-maven-plugin.version>3.0.0</build-helper-maven-plugin.version>
        <openapi-generator-maven-plugin.version>3.3.4</openapi-generator-maven-plugin.version>
        <maven-enforcer-plugin.version>1.4.1</maven-enforcer-plugin.version>
        <maven-shade-plugin>3.2.1</maven-shade-plugin>

        <junit.version>4.12</junit.version>
        <mockito.version>2.27.0</mockito.version>

        <lombok.version>1.18.6</lombok.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>io.vertx</groupId>
            <artifactId>vertx-core</artifactId>
            <version>${vertx.version}</version>
        </dependency>
        <dependency>
            <groupId>io.vertx</groupId>
            <artifactId>vertx-mysql-postgresql-client</artifactId>
            <version>${vertx.version}</version>
            <exclusions>
                <exclusion>
                    <groupId>org.scala-lang</groupId>
                    <artifactId>scala-library</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>io.vertx</groupId>
            <artifactId>vertx-web</artifactId>
            <version>${vertx.version}</version>
        </dependency>
        <dependency>
            <groupId>io.vertx</groupId>
            <artifactId>vertx-hazelcast</artifactId>
            <version>${vertx.version}</version>
        </dependency>
        <dependency>
            <groupId>io.vertx</groupId>
            <artifactId>vertx-auth-common</artifactId>
            <version>${vertx.version}</version>
        </dependency>
        <dependency>
            <groupId>io.vertx</groupId>
            <artifactId>vertx-config</artifactId>
            <version>${vertx.version}</version>
        </dependency>
        <dependency>
            <groupId>io.swagger</groupId>
            <artifactId>swagger-annotations</artifactId>
            <version>${swagger.version}</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${sl4j.version}</version>
        </dependency>
        <dependency>
            <groupId>org.bitbucket.b_c</groupId>
            <artifactId>jose4j</artifactId>
            <version>${jose4j.version}</version>
        </dependency>
        <dependency>
            <groupId>org.scala-lang</groupId>
            <artifactId>scala-library</artifactId>
            <version>${scala.version}</version>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>${junit.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <version>${mockito.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>io.vertx</groupId>
            <artifactId>vertx-unit</artifactId>
            <version>${vertx.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>${lombok.version}</version>
            <scope>provided</scope>
        </dependency>
    </dependencies>

    <build>
        <finalName>${project.artifactId}</finalName>
        <resources>
            <resource>
                <directory>${project.basedir}/src/main/resources</directory>
                <filtering>true</filtering>
            </resource>
        </resources>

        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven.compiler.plugin.version}</version>
                <configuration>
                    <parameters>true</parameters>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.openapitools</groupId>
                <artifactId>openapi-generator-maven-plugin</artifactId>
                <version>${openapi-generator-maven-plugin.version}</version>
                <executions>
                    <execution>
                        <id>swagger-api</id>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                        <configuration>
                            <inputSpec>${project.basedir}/src/main/resources/api-v1.yml</inputSpec>
                            <generatorName>java</generatorName>
                            <modelPackage>com.rogelioorts.assignments.redhat.users.service.model</modelPackage>
                            <modelNamePrefix>Api</modelNamePrefix>
                            <output>${project.build.directory}/generated-sources/open-api</output>
                            <addCompileSourceRoot>false</addCompileSourceRoot>
                            <generateApis>false</generateApis>
                            <generateSupportingFiles>false</generateSupportingFiles>
                            <configOptions>
                                <dateLibrary>java8</dateLibrary>
                                <java8>true</java8>
                                <interfaceOnly>true</interfaceOnly>
                                <useTags>true</useTags>
                                <library>vertx</library>
                            </configOptions>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>build-helper-maven-plugin</artifactId>
                <version>${build-helper-maven-plugin.version}</version>
                <executions>
                    <execution>
                        <id>add-source</id>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>add-source</goal>
                        </goals>
                        <configuration>
                            <sources>
                                <source>${project.build.directory}/generated-sources/open-api/src/main/java</source>
                            </sources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-enforcer-plugin</artifactId>
                <version>${maven-enforcer-plugin.version}</version>
                <executions>
                    <execution>
                        <id>enforce-versions</id>
                        <goals>
                            <goal>enforce</goal>
                        </goals>
                        <configuration>
                            <rules>
                                <dependencyConvergence/>
                                <requireJavaVersion>
                                    <version>1.8</version>
                                </requireJavaVersion>
                                <requireMavenVersion>
                                    <version>[3.0.3,)</version>
                                </requireMavenVersion>
                                <requireNoRepositories/>
                                <requirePluginVersions>
                                    <message>Best Practice is to always define plugin versions!</message>
                                    <banLatest>true</banLatest>
                                    <banSnapshots>true</banSnapshots>
                                    <unCheckedPluginList>
                                        org.apache.maven.plugins:maven-clean-plugin,
                                        org.apache.maven.plugins:maven-install-plugin,
                                        org.apache.maven.plugins:maven-site-plugin,
                                        org.apache.maven.plugins:maven-deploy-plugin,
                                        org.apache.maven.plugins:maven-resources-plugin,
                                        org.apache.maven.plugins:maven-surefire-plugin,
                                        org.apache.maven.plugins:maven-jar-plugin
                                    </unCheckedPluginList>
                                </requirePluginVersions>
                                <requireReleaseDeps>
                                    <onlyWhenRelease>true</onlyWhenRelease>
                                    <message>No snapshots allowed in release version!</message>
                                </requireReleaseDeps>
                            </rules>
                            <failFast>true</failFast>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <version>${maven-shade-plugin}</version>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                        <configuration>
                            <transformers>
                                <transformer
                                    implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                                    <mainClass>com.rogelioorts.assignments.redhat.users.service.MainVerticle</mainClass>
                                </transformer>
                            </transformers>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

</project>
