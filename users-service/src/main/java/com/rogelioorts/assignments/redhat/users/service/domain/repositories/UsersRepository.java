package com.rogelioorts.assignments.redhat.users.service.domain.repositories;

import com.rogelioorts.assignments.redhat.users.service.domain.models.DomainUser;
import io.vertx.core.Future;


public interface UsersRepository {

    Future<DomainUser> findByUsername(String username);

    Future<Void> sumOneToNumTasks(String username);

}
