package com.rogelioorts.assignments.redhat.users.service.infrastructure.mappers;

import com.rogelioorts.assignments.redhat.users.service.domain.models.DomainUser;
import io.vertx.core.json.JsonObject;

public final class UsersInfraMapper {

    private UsersInfraMapper() {
    }

    public static DomainUser toDomain(final JsonObject row) {
        final DomainUser user = new DomainUser();
        user.setUsername(row.getString("username"));
        user.setPassword(row.getString("password"));
        user.setSalt(row.getString("salt"));
        user.setNumTasks(row.getInteger("num_tasks"));

        return user;
    }

}
