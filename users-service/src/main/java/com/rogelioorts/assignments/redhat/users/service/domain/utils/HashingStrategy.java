package com.rogelioorts.assignments.redhat.users.service.domain.utils;

public interface HashingStrategy {

    String hash(String password, String salt);

    String generateSalt();

}
