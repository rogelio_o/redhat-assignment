package com.rogelioorts.assignments.redhat.users.service.rest.mappers;

import com.rogelioorts.assignments.redhat.users.service.domain.models.DomainUser;
import com.rogelioorts.assignments.redhat.users.service.model.ApiUser;

public final class UsersRestMapper {

    private UsersRestMapper() {
    }

    public static ApiUser toRest(final DomainUser domainUser) {
        final ApiUser restUser = new ApiUser();
        restUser.setUsername(domainUser.getUsername());
        restUser.setNumTasks(domainUser.getNumTasks());

        return restUser;
    }

}
