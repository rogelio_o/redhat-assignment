package com.rogelioorts.assignments.redhat.users.service.infrastructure.repositories;

import com.rogelioorts.assignments.redhat.users.service.domain.models.DomainUser;
import com.rogelioorts.assignments.redhat.users.service.domain.repositories.UsersRepository;
import com.rogelioorts.assignments.redhat.users.service.infrastructure.mappers.UsersInfraMapper;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.sql.SQLConnection;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class UsersSqlRepository implements UsersRepository {

    private static final String FIND_BY_USERNAME_SQL = "SELECT * FROM users WHERE username = ?";

    private static final String SUM_ONE_TO_NUM_TASKS = "UPDATE users SET num_tasks = num_tasks + 1 WHERE username = ?";

    private final SQLClient client;

    @Override
    public Future<DomainUser> findByUsername(final String username) {
        return getConnection().compose((connection) -> {
            final Future<DomainUser> f = Future.future();

            connection.queryWithParams(
                FIND_BY_USERNAME_SQL,
                new JsonArray()
                    .add(username),
                sqlRes -> {
                    connection.close();

                    if (sqlRes.succeeded()) {
                        final ResultSet resultSet = sqlRes.result();

                        final List<JsonObject> rows = resultSet.getRows();
                        if (rows.isEmpty()) {
                            f.complete();
                        } else {
                            f.complete(UsersInfraMapper.toDomain(rows.get(0)));
                        }
                    } else {
                        f.fail(sqlRes.cause());
                    }
                });

            return f;
        });
    }

    @Override
    public Future<Void> sumOneToNumTasks(final String username) {
        return getConnection().compose((connection) -> {
            final Future<Void> f = Future.future();

            connection.updateWithParams(
                SUM_ONE_TO_NUM_TASKS,
                new JsonArray()
                    .add(username),
                sqlRes -> {
                    connection.close();

                    if (sqlRes.succeeded()) {
                        f.complete();
                    } else {
                        f.fail(sqlRes.cause());
                    }
                });

            return f;
        });
    }

    private Future<SQLConnection> getConnection() {
        final Future<SQLConnection> f = Future.future();

        client.getConnection(res -> {
            if (res.succeeded()) {
                f.complete(res.result());
            } else {
                f.fail(res.cause());
            }
        });

        return f;
    }

}
