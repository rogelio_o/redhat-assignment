package com.rogelioorts.assignments.redhat.users.service.domain.services.impl;

import com.rogelioorts.assignments.redhat.users.service.domain.models.DomainUser;
import com.rogelioorts.assignments.redhat.users.service.domain.repositories.UsersRepository;
import com.rogelioorts.assignments.redhat.users.service.domain.services.UsersService;
import com.rogelioorts.assignments.redhat.users.service.domain.utils.HashingStrategy;
import io.vertx.core.Future;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;

    private final HashingStrategy hashingService;

    @Override
    public Future<Boolean> checkUsernameAndPassword(final String username, final String password) {
        return usersRepository.findByUsername(username).map(user -> {
            if (user == null) {
                return false;
            } else {
                return hashingService.hash(password, user.getSalt()).equals(user.getPassword());
            }
        });
    }

    @Override
    public Future<DomainUser> findUserById(final String username) {
        return usersRepository.findByUsername(username);
    }

}
