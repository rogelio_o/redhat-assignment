package com.rogelioorts.assignments.redhat.users.service.event.bus.listeners;

import com.rogelioorts.assignments.redhat.users.service.domain.repositories.UsersRepository;
import com.rogelioorts.assignments.redhat.users.service.event.bus.models.TaskCreatedEvent;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;

import java.util.logging.Level;

@RequiredArgsConstructor
@Log
public class TaskCreatedListener implements Handler<Message<JsonObject>> {

    private final UsersRepository usersRepository;

    @Override
    public void handle(final Message<JsonObject> jsonObjectMessage) {
        log.log(Level.FINER, getEventId() + " event received.");

        final TaskCreatedEvent event = jsonObjectMessage.body().mapTo(TaskCreatedEvent.class);

        usersRepository.sumOneToNumTasks(event.getUsername()).setHandler(res -> {
            if (res.succeeded()) {
                log.log(Level.FINER, "Task added to the count of " + event.getUsername());
            } else {
                log.log(Level.SEVERE, "Error adding task to user " + event.getUsername(), res.cause());
            }
        });
    }

    public String getEventId() {
        return "task-created";
    }

}
