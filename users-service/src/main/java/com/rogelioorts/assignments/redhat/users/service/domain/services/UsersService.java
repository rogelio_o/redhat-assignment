package com.rogelioorts.assignments.redhat.users.service.domain.services;

import com.rogelioorts.assignments.redhat.users.service.domain.models.DomainUser;
import io.vertx.core.Future;

public interface UsersService {

    Future<Boolean> checkUsernameAndPassword(String username, String password);

    Future<DomainUser> findUserById(String username);

}
