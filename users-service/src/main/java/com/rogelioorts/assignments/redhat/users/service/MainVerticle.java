package com.rogelioorts.assignments.redhat.users.service;

import com.rogelioorts.assignments.redhat.users.service.domain.repositories.UsersRepository;
import com.rogelioorts.assignments.redhat.users.service.domain.services.UsersService;
import com.rogelioorts.assignments.redhat.users.service.domain.services.impl.UsersServiceImpl;
import com.rogelioorts.assignments.redhat.users.service.domain.utils.HashingStrategy;
import com.rogelioorts.assignments.redhat.users.service.event.bus.listeners.TaskCreatedListener;
import com.rogelioorts.assignments.redhat.users.service.infrastructure.repositories.UsersSqlRepository;
import com.rogelioorts.assignments.redhat.users.service.infrastructure.utils.SHA512HashingStrategy;
import com.rogelioorts.assignments.redhat.users.service.rest.config.JoseAuthHandler;
import com.rogelioorts.assignments.redhat.users.service.rest.config.JoseAuthProvider;
import com.rogelioorts.assignments.redhat.users.service.rest.controllers.UsersController;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.*;
import io.vertx.core.eventbus.EventBusOptions;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.asyncsql.PostgreSQLClient;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import lombok.extern.java.Log;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;

@Log
public class MainVerticle extends AbstractVerticle {

    private static final int PORT = 8082;

    private JsonObject config = new JsonObject();

    @Override
    public void start(final Future<Void> startFuture) {
        loadConfiguration(loadRes -> {
            try {
                loadBeans(beansRes -> {
                    if (beansRes.succeeded()) {
                        startWithBeans(beansRes.result(), startFuture);
                    } else {
                        beansRes.cause().printStackTrace();
                    }
                });
            } catch (Exception e) {
                log.log(Level.SEVERE, "Error loading beans.", e);
                startFuture.fail(e);
                vertx.close();
            }
        });
    }

    private void loadConfiguration(Handler<AsyncResult<Void>> handler) {
        ConfigStoreOptions env = new ConfigStoreOptions()
            .setType("env");

        ConfigRetrieverOptions options = new ConfigRetrieverOptions()
            .addStore(env);

        ConfigRetriever retriever = ConfigRetriever.create(vertx, options);
        retriever.getConfig(configRes -> {
            if (configRes.succeeded()) {
                config = configRes.result();

                handler.handle(Future.succeededFuture());
            } else {
                handler.handle(Future.failedFuture(configRes.cause()));
            }
        });
    }

    private void loadBeans(final Handler<AsyncResult<Map<String, Object>>> handler) throws Exception {
        final Map<String, Object> beans = new ConcurrentHashMap();

        final JsonObject postgreSQLClientConfig = new JsonObject()
            .put("host", config.getString("POSTGRESQL_HOST", "localhost"))
            .put("username", "postgres")
            .put("database", "users");
        final SQLClient postgreSQLClient = PostgreSQLClient.createShared(vertx, postgreSQLClientConfig);
        beans.put("SQLClient", postgreSQLClient);

        final HashingStrategy hashingStrategy = new SHA512HashingStrategy(vertx);
        beans.put("HashingStrategy", hashingStrategy);

        final UsersRepository usersRepository = new UsersSqlRepository(postgreSQLClient);
        beans.put("UsersRepository", usersRepository);

        final UsersService usersService = new UsersServiceImpl(usersRepository, hashingStrategy);
        beans.put("UsersService", usersService);

        final UsersController usersController = new UsersController(usersService);
        beans.put("UsersController", usersController);

        final JoseAuthProvider joseAuthProvider = new JoseAuthProvider(JoseAuthProvider
            .createJWTConsumer(config.getString("JWT_VERIFIER_KEY_URI", "http://localhost:8081/tokens/jwt/key")));
        beans.put("JoseAuthProvider", joseAuthProvider);

        final JoseAuthHandler joseAuthHandler = new JoseAuthHandler(joseAuthProvider);
        beans.put("JoseAuthHandler", joseAuthHandler);

        final TaskCreatedListener taskCreatedListener = new TaskCreatedListener(usersRepository);
        beans.put("TaskCreatedListener", taskCreatedListener);

        handler.handle(Future.succeededFuture(beans));
    }

    private void startWithBeans(final Map<String, Object> beans, final Future<Void> startFuture) {
        final Future<Void> fHttpServer = Future.future();
        createHttpServer(beans, fHttpServer);

        final Future<Void> fEventBus = Future.future();
        addListenersToEventBus(beans, fEventBus);

        CompositeFuture.all(Arrays.asList(fHttpServer, fEventBus)).setHandler(res -> {
            if (res.succeeded()) {
                startFuture.complete();
            } else {
                startFuture.fail(res.cause());
            }
        });
    }

    private void createHttpServer(final Map<String, Object> beans, final Future<Void> future) {
        final HttpServer server = vertx.createHttpServer();

        final Router router = createRouter(beans);

        final int port = getServerPort();
        server.requestHandler(router).listen(port, serverRes -> {
            log.info("Server started in " + port + ".");
            future.complete();
        });
    }

    private Router createRouter(final Map<String, Object> beans) {
        final Router router = Router.router(vertx);
        final JoseAuthHandler joseAuthHandler = (JoseAuthHandler) beans.get("JoseAuthHandler");

        router.route().handler(BodyHandler.create());

        final UsersController usersController = (UsersController) beans.get("UsersController");
        router
            .route(usersController.getCurrentUserMethod(), usersController.getCurrentUserPath())
            .handler(joseAuthHandler);
        router
            .route(usersController.getCurrentUserMethod(), usersController.getCurrentUserPath())
            .handler(usersController::getCurrentUser);
        router
            .route(usersController.validateUserLoginMethod(), usersController.validateUserLoginPath())
            .handler(usersController::validateUserLogin);

        return router;
    }

    private int getServerPort() {
        return config.getInteger("PORT", PORT);
    }

    private void addListenersToEventBus(Map<String, Object> beans, final Future<Void> future) {
        final TaskCreatedListener taskCreatedListener = (TaskCreatedListener) beans.get("TaskCreatedListener");
        vertx.eventBus().consumer(taskCreatedListener.getEventId(), taskCreatedListener);

        future.complete();
    }

    public static void main(final String[] args) {
        final EventBusOptions eventBusOptions = new EventBusOptions();
        final String hostName = System.getenv("HOST_NAME");
        if (hostName != null) {
            eventBusOptions.setHost(hostName);
        }
        final VertxOptions options = new VertxOptions()
            .setEventBusOptions(eventBusOptions);
        Vertx.clusteredVertx(options, res -> {
            if (res.succeeded()) {
                Vertx vertx = res.result();
                vertx.deployVerticle(new MainVerticle());
            } else {
                log.log(Level.SEVERE, "Error getting clustered Vert.x", res.cause());
            }
        });
    }

}
