package com.rogelioorts.assignments.redhat.users.service.domain.models;

import lombok.Data;

@Data
public class DomainUser {

    private String username;

    private String password;

    private String salt;

    private long numTasks;

}
