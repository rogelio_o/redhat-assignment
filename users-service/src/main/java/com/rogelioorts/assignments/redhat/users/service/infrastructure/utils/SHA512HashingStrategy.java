package com.rogelioorts.assignments.redhat.users.service.infrastructure.utils;

import com.rogelioorts.assignments.redhat.users.service.domain.utils.HashingStrategy;
import io.vertx.core.Vertx;
import io.vertx.ext.auth.PRNG;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SHA512HashingStrategy implements HashingStrategy {

    private final char[] HEX_CHARS = "0123456789ABCDEF".toCharArray();

    private final PRNG random;

    private final MessageDigest md;

    public SHA512HashingStrategy(final Vertx vertx) {
        random = new PRNG(vertx);

        try {
            md = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException nsae) {
            throw new RuntimeException("SHA-512 is not available", nsae);
        }
    }

    @Override
    public String hash(final String password, final String salt) {
        final String concat = (salt == null ? "" : salt) + password;
        final byte[] bHash = md.digest(concat.getBytes(StandardCharsets.UTF_8));

        return bytesToHex(bHash);
    }

    @Override
    public String generateSalt() {
        final byte[] salt = new byte[32];
        random.nextBytes(salt);

        return bytesToHex(salt);
    }

    private String bytesToHex(final byte[] bytes) {
        final char[] chars = new char[bytes.length * 2];
        for (int i = 0; i < bytes.length; i++) {
            final int x = 0xFF & bytes[i];
            chars[i * 2] = HEX_CHARS[x >>> 4];
            chars[1 + i * 2] = HEX_CHARS[0x0F & x];
        }

        return new String(chars);
    }

}
