package com.rogelioorts.assignments.redhat.users.service.rest.controllers;

import com.rogelioorts.assignments.redhat.users.service.domain.services.UsersService;
import com.rogelioorts.assignments.redhat.users.service.model.ApiLoginForm;
import com.rogelioorts.assignments.redhat.users.service.model.ApiLoginValidationResult;
import com.rogelioorts.assignments.redhat.users.service.model.ApiUser;
import com.rogelioorts.assignments.redhat.users.service.rest.mappers.UsersRestMapper;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UsersController {

    private final UsersService usersService;

    public void getCurrentUser(final RoutingContext context) {
        usersService.findUserById(context.user().principal().getString("sub"))
            .setHandler(res -> {
                if (res.succeeded()) {
                    final ApiUser user = UsersRestMapper.toRest(res.result());
                    context.response()
                        .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                        .end(JsonObject.mapFrom(user).toBuffer());
                } else {
                    res.cause().printStackTrace();
                    context.fail(res.cause());
                }
            });
    }

    public String getCurrentUserPath() {
        return "/current";
    }

    public HttpMethod getCurrentUserMethod() {
        return HttpMethod.GET;
    }

    public void validateUserLogin(final RoutingContext context) {
        final ApiLoginForm loginForm = context.getBodyAsJson().mapTo(ApiLoginForm.class);

        usersService.checkUsernameAndPassword(loginForm.getUsername(), loginForm.getPassword())
            .setHandler(res -> {
                if (res.succeeded()) {
                    final ApiLoginValidationResult loginValidationResult = new ApiLoginValidationResult();
                    loginValidationResult.setResult(res.result());

                    context.response()
                        .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                        .end(JsonObject.mapFrom(loginValidationResult).toBuffer());
                } else {
                    res.cause().printStackTrace();
                    context.fail(res.cause());
                }
            });
    }

    public String validateUserLoginPath() {
        return "/validate-login";
    }

    public HttpMethod validateUserLoginMethod() {
        return HttpMethod.POST;
    }

}
