package com.rogelioorts.assignments.redhat.users.service.domain.services.impl;

import com.rogelioorts.assignments.redhat.users.service.domain.models.DomainUser;
import com.rogelioorts.assignments.redhat.users.service.domain.repositories.UsersRepository;
import com.rogelioorts.assignments.redhat.users.service.domain.utils.HashingStrategy;
import io.vertx.core.Future;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(VertxUnitRunner.class)
public class UsersServiceImplTest {

    private final static String USERNAME = "test-username";
    private final static String NOT_EXISTING_USERNAME = "unknown-username";
    private final static String PASSWORD = "password";
    private final static String WRONG_PASSWORD = "wrong-password";
    private final static String SALT = "4D0DE36D414C249301B6A688C469C2769BD6F736A2926922F0153407FE527A03";

    private UsersServiceImpl usersService;

    @Before
    public void initializeUsersService() {
        usersService = new UsersServiceImpl(createUsersRepository(), createHashingStrategy());
    }

    public HashingStrategy createHashingStrategy() {
        final HashingStrategy hashingStrategy = Mockito.mock(HashingStrategy.class);
        when(hashingStrategy.hash(anyString(), anyString()))
            .then(answer -> hash(answer.getArgument(0, String.class), answer.getArgument(1, String.class)));
        when(hashingStrategy.generateSalt()).thenReturn(SALT);

        return hashingStrategy;
    }

    public UsersRepository createUsersRepository() {
        final DomainUser user = new DomainUser();
        user.setSalt(SALT);
        user.setPassword(hash(PASSWORD, user.getSalt()));

        final UsersRepository usersRepository = Mockito.mock(UsersRepository.class);
        when(usersRepository.findByUsername(USERNAME)).thenReturn(Future.succeededFuture(user));
        when(usersRepository.findByUsername(NOT_EXISTING_USERNAME)).thenReturn(Future.succeededFuture());

        return usersRepository;
    }

    private String hash(final String password, final String salt) {
        return password + salt;
    }

    @Test
    public void testCheckUsernameAndPasswordWithNotExistingUserReturnsFalse(final TestContext context) {
        final Async async = context.async();
        usersService.checkUsernameAndPassword(NOT_EXISTING_USERNAME, PASSWORD).setHandler(res -> {
            if (res.succeeded()) {
                context.assertFalse(res.result());

                async.complete();
            } else {
                context.fail(res.cause());
            }
        });
    }

    @Test
    public void testCheckUsernameAndPasswordWithNotMatchingPasswordReturnsFalse(final TestContext context) {
        final Async async = context.async();
        usersService.checkUsernameAndPassword(USERNAME, WRONG_PASSWORD).setHandler(res -> {
            if (res.succeeded()) {
                context.assertFalse(res.result());

                async.complete();
            } else {
                context.fail(res.cause());
            }
        });
    }

    @Test
    public void testCheckUsernameAndPasswordWithMatchingPasswordReturnsTrue(final TestContext context) {
        final Async async = context.async();
        usersService.checkUsernameAndPassword(USERNAME, PASSWORD).setHandler(res -> {
            if (res.succeeded()) {
                context.assertTrue(res.result());

                async.complete();
            } else {
                context.fail(res.cause());
            }
        });
    }

}
