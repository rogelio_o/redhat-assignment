package com.rogelioorts.assignments.redhat.auth.service.controllers;

import com.rogelioorts.assignments.redhat.auth.service.mappers.JWSKeyMapper;
import com.rogelioorts.assignments.redhat.auth.service.model.ApiJWSKeys;
import com.rogelioorts.assignments.redhat.auth.service.services.TokenConverterService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
public class TokensJwtController implements TokensJwtApi {

    private final TokenConverterService tokenConverterService;

    @Override
    public ResponseEntity<String> convertJwtToken(final String token) {
        try {
            final Optional<String> optionalJWT = tokenConverterService.toJWT(token);
            if (optionalJWT.isPresent()) {
                return ResponseEntity.ok(optionalJWT.get());
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ResponseEntity<ApiJWSKeys> jwtPublicKey() {
        final Map<String, ?> publicKey = tokenConverterService.getPublicKey();

        return ResponseEntity.ok(JWSKeyMapper.toApi(publicKey));
    }

}
