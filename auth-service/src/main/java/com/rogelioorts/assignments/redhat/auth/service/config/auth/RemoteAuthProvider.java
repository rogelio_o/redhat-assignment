package com.rogelioorts.assignments.redhat.auth.service.config.auth;

import lombok.Builder;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

@Component
public class RemoteAuthProvider implements AuthenticationProvider {

    private final RestTemplate restTemplate;

    private final String validateUserLoginUrl;

    public RemoteAuthProvider(final RestTemplate restTemplate,
                              @Value("${auth.validate-user-login-url}") final String validateUserLoginUrl) {
        this.restTemplate = restTemplate;
        this.validateUserLoginUrl = validateUserLoginUrl;
    }

    @Override
    public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
        final String name = authentication.getName();
        final String password = authentication.getCredentials().toString();

        final RemoteResponse response = restTemplate.postForObject(
            validateUserLoginUrl,
            RemoteRequest.builder().username(name).password(password).build(),
            RemoteResponse.class);

        if (response.isResult()) {
            return new UsernamePasswordAuthenticationToken(name, password, new ArrayList<>());
        } else {
            throw new BadCredentialsException("The username or the password is not correct.");
        }
    }

    @Override
    public boolean supports(final Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    @Data
    @Builder
    private static class RemoteRequest {

        private String username;

        private String password;

    }

    @Data
    private static class RemoteResponse {

        private boolean result;

    }

}
