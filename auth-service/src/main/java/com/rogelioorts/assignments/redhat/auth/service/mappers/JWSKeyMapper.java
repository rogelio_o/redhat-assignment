package com.rogelioorts.assignments.redhat.auth.service.mappers;

import com.rogelioorts.assignments.redhat.auth.service.model.ApiJWSKey;
import com.rogelioorts.assignments.redhat.auth.service.model.ApiJWSKeys;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class JWSKeyMapper {

    private JWSKeyMapper() {
    }

    public static ApiJWSKeys toApi(final Map<String, ?> publicKey) {
        final List<Map<String, ?>> keys = (List<Map<String, ?>>) publicKey.get("keys");

        final ApiJWSKeys apiJWSKeys = new ApiJWSKeys();
        apiJWSKeys.setKeys(keys.stream().map(JWSKeyMapper::toApiKey).collect(Collectors.toList()));

        return apiJWSKeys;
    }

    private static ApiJWSKey toApiKey(final Map<String, ?> publicKey) {
        final ApiJWSKey apiJWSKey = new ApiJWSKey();
        apiJWSKey.setKty((String) publicKey.get("kty"));
        apiJWSKey.setKid((String) publicKey.get("kid"));
        apiJWSKey.setN((String) publicKey.get("n"));
        apiJWSKey.setE((String) publicKey.get("e"));

        return apiJWSKey;
    }

}
