package com.rogelioorts.assignments.redhat.auth.service.services.impl;

import com.rogelioorts.assignments.redhat.auth.service.services.TokenConverterService;
import lombok.RequiredArgsConstructor;
import org.jose4j.jwk.JsonWebKey;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.lang.JoseException;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TokenConverterServiceImpl implements TokenConverterService {

    private static final String ISSUER = "redhat-assignment";

    private final TokenStore tokenStore;

    private final RsaJsonWebKey rsaJsonWebKey;

    @Override
    public Optional<String> toJWT(final String bearerToken) throws Exception {
        final OAuth2Authentication authentication = tokenStore.readAuthentication(bearerToken);
        if (authentication == null) {
            return Optional.empty();
        }

        final OAuth2AccessToken accessToken = tokenStore.getAccessToken(authentication);

        final JwtClaims claims = getClaimsFromAuthenticationAndAccessToken(authentication, accessToken);
        return Optional.of(getJWTFromClaims(claims));
    }

    private JwtClaims getClaimsFromAuthenticationAndAccessToken(final OAuth2Authentication authentication,
                                                                final OAuth2AccessToken accessToken) {
        final JwtClaims claims = new JwtClaims();
        claims.setIssuer(ISSUER);
        claims.setExpirationTimeMinutesInTheFuture(accessToken.getExpiresIn());
        claims.setGeneratedJwtId();
        claims.setIssuedAtToNow();
        claims.setSubject(authentication.getName());
        claims.setClaim("client_id", authentication.getOAuth2Request().getClientId());

        final List<String> scopes = accessToken.getScope().stream().collect(Collectors.toList());
        claims.setStringListClaim("scope", scopes);

        return claims;
    }

    private String getJWTFromClaims(final JwtClaims claims) throws JoseException {
        final JsonWebSignature jws = new JsonWebSignature();
        jws.setPayload(claims.toJson());
        jws.setKey(rsaJsonWebKey.getPrivateKey());
        jws.setKeyIdHeaderValue(rsaJsonWebKey.getKeyId());
        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);

        return jws.getCompactSerialization();
    }

    @Override
    public Map<String, Object> getPublicKey() {
        final Map<String, Object> key = rsaJsonWebKey.toParams(JsonWebKey.OutputControlLevel.PUBLIC_ONLY);
        final List<Object> keys = Arrays.asList(key);
        final Map<String, Object> result = new HashMap<>();
        result.put("keys", keys);

        return result;
    }

}
