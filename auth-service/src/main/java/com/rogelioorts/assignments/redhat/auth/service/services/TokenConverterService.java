package com.rogelioorts.assignments.redhat.auth.service.services;

import java.util.Map;
import java.util.Optional;

public interface TokenConverterService {

    Optional<String> toJWT(String bearerToken) throws Exception;

    Map<String, Object> getPublicKey();

}
