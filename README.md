# Red Hat Assignment

## Overall architecture

In the image below it can be seen how the system is composed by four
different services, each one with a single responsibility. 

The idea is that the only way to access the system is through the 
gateway-service. The gateway-service controls what is exposed to the 
exterior of the system and, thanks to this, it privatizes the 
internal endpoints.

There are exposed endpoints that can only be consumed by logged in
users. The way to securing these endpoints is through Oauth2 protocol.
The Oauth2 protocol is managed by the auth-service, which returns a
bearer token to be used in each protected call. Because the bearer 
token does not identify a user, a JWT token is used internally. The
gateway-service converts the bearer token to a JWT signed token
with the help of the auth-service.

Each service has its own database. _auth-service_ has no database and
store the Oauth2 data (clients, tokens,...) on memory. _tasks-service_
uses a MongoDB database because task model has a documentary nature
and queries and writing are simpler with this database. _users-service_
uses a PostgreSQL because there is no special consideration to bear
in mind and in these cases a well tested SQL solution is preferable.

The _users-service_ consumes an event of the _tasks-service_: "task created".
Users have a tasks counter and, instead of asking it directly to the
_tasks-service_, the _users-service_ increase the tasks counter each time
a "task created" event is received. This communication is made using
the Vert.x's event bus and Hazelcast as cluster manager.

It is important to consider that in this early stage of the project without
further information it is complex to choose the right tools. Maybe
MongoDB or the event bus are not the right choose. However, the important
thing here is that our project makes easy to change these decision without
changing any business logic thanks to the use of the "Port and Adapters"
pattern.

**Important consideration:** auth-service and users-service are
highly coupled and a improvement of the architecture would be
move users-service features to auth-service.

**Important consideration:** "consumers groups" feature is not 
available in the Vert.x's event bus. So if it exists _n_ 
instances of _users-service_, the tasks counter is increased 
by _n_ because all the instances would receive the event. This could
be fixed developing a custom component or exploring other tools
like Kafka.

**Important consideration:** Other tools like Kong could be used instead of
the _gateway-service_ and the _auth-service_ but, because this is an 
assignment, it has been try to develop the whole solution.

```
                           ^
                           | http://localhost:8080
+--------------------------+-------------------------+
|                                                    |
|                   gateway-service                  |
|                                                    |
+--------^-----------------^----------------^--------+
         | /users          | /auth          | /tasks
+--------+------+ +--------+------+ +-------+--------+
|               | |               | |                |
| users-service | | auth-service  | | tasks-service  |
|               | |               | |                |
+-------^-------+ +---------------+ +---------^------+
        |                                     |
+-------+-------+                    +--------+------+
|               |                    |               |
|   postgresql  |                    |    mongodb    |
|               |                    |               |
+---------------+                    +---------------+
```

## How to run it

In order to make easy the building process, it exists an _umbrella_ 
pom.xml with which all the services can be built. In the project root
folder the next command has to be executed:

```
mvn clean package
```

Once the services are built, they can be deployed to Docker containers
using _docker-compose_. All the databases and required configurations 
are in the docker-compose.yml file, so just executing the next command
all the system will be up:

```
docker-compose up --build
```

## How to use it

First thing is needed to start using the system is a Bearer token. To make
it easy, the _implicit_ Oauth2 flow is allowed (_code_ flow is also
allowed).

Follow the next step to get a token:
1. Visit 
<http://localhost:8080/auth/oauth/authorize?client_id=test-client&response_type=token&redirect_url=http://localhost:8080/cb>
2. Log in with [username: _user_, password: _password_] 
(or [username: _user2_, password: _password2_]).
3. Authorize the scopes.
4. You will be redirected to the _redirect_url_. This URL does not exist
but you can find the token in the _access_token_ parameter of the URL.
5. Save the token in a environment variable, we will use it later:
```export TOKEN="..."```.

A _Postman_ collection with all the external endpoints has been exported 
in the file postman-v2.json (and postman-v2.1.json). There is also 
further information about the endpoints (externals and internals) in 
the OpenAPI spec of each service. The OpenAPI spec can be found 
in the src/main/resources/api-v1.yml file of each service. In the
following section the features are discussed in details and _curl_
commands are provided to check each one.

## Services features

### gateway-service

It has been developed with Spring Cloud Gateway, a reactive gateway build
over Netty. A custom filter has been added to transforms the Bearer token
into a JWT token. The filter calls the _auth-server_ with the Bearer token,
which converts the token.

**Features:**
- It routes each request to the right service.
- It converts the bearer token to a JWT token in the requests that
are made to users-service and tasks-service.

### auth-service

It uses the Oauth2 Authorization Server library of Spring. The authorization
service is created automatically using injected beans. This service is 
not reactive because the library does not support Spring webflux yet.

It exists a custom component to check the user login form through the
_users-service_.

This service also transforms the Bearer token in a JWT token through
an endpoint. The JWT is signed with a private key and a public key
that are generated randomly when the service is starting. The public
key is shared in an endpoint that are consume by the services that
want to verify the signed JWT.

**Important consideration:** if it exists more than one _auth-service_,
each one would have its own signing key. So, the services will not been 
able to check the JWT's signature. There is also a problem related with this:
if the _auth-service_ crashes, when it starts again it has new keys.
Because the others services does not updates the key used to verify the
JWT, the verification will fail for each token signed by the restarted
service. A solution for these situations would be use not-random keys
that could be in an _auth-service_ file.

### users-service

The _users-service_ has been developed using Vert.x.

An endpoint is exposed to verify users login forms, but can be only accessed
inside the system.

Other endpoint is exposed to get the "current user" data. The "current user"
is known using the JWT, so the Bearer token is required to make the call.

```
curl -H "Authorization:Bearer $TOKEN" "http://localhost:8080/users/current"
```

As talked previously, the tasks counter is updated with the "task created"
event.

### tasks-service

The _tasks-service_ has been developed using Vert.x.

All the endpoints of this service are exposed outside the system and all
of them require a Bearer token to be accessed.

- **create new task:** if a task is created successfully, a "task created"
event is published.

```
curl -d '{"title":"new task","description":"a long task description","tags":["important"],"done":false}' -X POST -H "Authorization:Bearer $TOKEN" -H "Content-Type:application/json" "http://localhost:8080/tasks/"
```

- **retrieve last tasks:** if the _tag_ query parameter is added, only the
tasks containing the given tag are shown.

```
curl -H "Authorization:Bearer $TOKEN" "http://localhost:8080/tasks/"
```

- **watch tasks:** this endpoint keeps alive the connection and listen each
published "task created" event. When a new event incomes, it is write
through the alive connection. It only writes the new tasks of the
logged in user. This communication way is called Server-Sent Event (SSE).

```
curl -v --http2 -N -H "Accept:text/event-stream" -H "Authorization:Bearer $TOKEN" "http://localhost:8080/tasks/watch"
```

## TODO

- Only domain unit tests have been developed, unit tests for 
infrastructure layer and presentation layer are required. 
Integration tests are also required.

- _auth-service_ and _users-service_ should be merged. It exists
a high coupling between them.

- Fixing the issue with the generated keys described in 
_auth-service_ section.

- Checking and fixing the technical debt with a tool like SonarQube.

- Code duplication removing. It exists duplicate components that could be 
shared between service, like the one which verifies the JWT.
