package com.rogelioorts.assignments.redhat.tasks.service.domain.repositories;

import com.rogelioorts.assignments.redhat.tasks.service.domain.models.Task;
import io.vertx.core.Future;

import java.util.List;

public interface TasksRepository {

    Future<Task> createTask(Task task);

    Future<List<Task>> getLastTasksOfUser(int limit, String username);

    Future<List<Task>> getLastTasksOfUserWithTag(int limit, String username, String tag);

}
