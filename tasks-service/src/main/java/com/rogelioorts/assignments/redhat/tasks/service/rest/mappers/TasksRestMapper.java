package com.rogelioorts.assignments.redhat.tasks.service.rest.mappers;

import com.rogelioorts.assignments.redhat.tasks.service.domain.models.Task;
import com.rogelioorts.assignments.redhat.tasks.service.event.bus.models.TaskCreatedEvent;
import com.rogelioorts.assignments.redhat.users.service.model.ApiListTasks;
import com.rogelioorts.assignments.redhat.users.service.model.ApiTask;
import com.rogelioorts.assignments.redhat.users.service.model.ApiTaskForm;

import java.util.List;
import java.util.stream.Collectors;

public final class TasksRestMapper {

    private TasksRestMapper() {
    }

    public static ApiListTasks toRest(final List<Task> tasks) {
        final ApiListTasks listTasks = new ApiListTasks();
        listTasks.setTasks(tasks.stream().map(TasksRestMapper::toRest).collect(Collectors.toList()));

        return listTasks;
    }

    public static ApiTask toRest(final Task task) {
        final ApiTask restTask = new ApiTask();
        restTask.setId(task.getId());
        restTask.setUsername(task.getUsername());
        restTask.setTitle(task.getTitle());
        restTask.setDescription(task.getDescription());
        restTask.setTags(task.getTags());
        restTask.setDone(task.getDone());

        return restTask;
    }

    public static Task toDomain(final ApiTaskForm form, final String username) {
        final Task task = new Task();
        task.setUsername(username);
        task.setTitle(form.getTitle());
        task.setDescription(form.getDescription());
        task.setTags(form.getTags());
        task.setDone(form.getDone());

        return task;
    }

    public static ApiTask toRest(final TaskCreatedEvent taskCreatedEvent) {
        final ApiTask apiTask = new ApiTask();
        apiTask.setId(taskCreatedEvent.getId());
        apiTask.setUsername(taskCreatedEvent.getUsername());
        apiTask.setTitle(taskCreatedEvent.getTitle());
        apiTask.setDescription(taskCreatedEvent.getDescription());
        apiTask.setTags(taskCreatedEvent.getTags());
        apiTask.setDone(taskCreatedEvent.isDone());

        return apiTask;
    }

}
