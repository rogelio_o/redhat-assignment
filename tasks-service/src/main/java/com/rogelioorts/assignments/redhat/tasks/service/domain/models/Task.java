package com.rogelioorts.assignments.redhat.tasks.service.domain.models;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
public class Task {

    private String id;

    private String username;

    private String title;

    private String description;

    private List<String> tags = new ArrayList<>();

    private Boolean done;

    private LocalDateTime creationDate;

}
