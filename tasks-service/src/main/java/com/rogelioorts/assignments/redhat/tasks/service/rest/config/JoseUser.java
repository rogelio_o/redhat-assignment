package com.rogelioorts.assignments.redhat.tasks.service.rest.config;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AbstractUser;
import io.vertx.ext.auth.AuthProvider;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class JoseUser extends AbstractUser {

    private final JsonObject claims;

    @Override
    protected void doIsPermitted(String s, Handler<AsyncResult<Boolean>> handler) {
        final JsonArray scopes = claims.getJsonArray("scope");

        if (scopes.contains(s)) {
            handler.handle(Future.succeededFuture(true));
        } else {
            handler.handle(Future.succeededFuture(false));
        }
    }

    @Override
    public JsonObject principal() {
        return claims;
    }

    @Override
    public void setAuthProvider(AuthProvider authProvider) {

    }

}
