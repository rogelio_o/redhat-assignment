package com.rogelioorts.assignments.redhat.tasks.service.rest.config;

import io.vertx.core.Handler;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class JoseAuthHandler implements Handler<RoutingContext> {

    private final JoseAuthProvider authProvider;

    @Override
    public void handle(final RoutingContext context) {
        final String headerValue = context.request().getHeader(HttpHeaders.AUTHORIZATION);
        if (headerValue == null) {
            context.fail(401);
        } else {
            authProvider.authenticate(new JsonObject().put("jwt", headerValue), res -> {
                if (res.succeeded()) {
                    context.setUser(res.result());

                    context.next();
                } else {
                    context.fail(401);
                }
            });
        }
    }

}
