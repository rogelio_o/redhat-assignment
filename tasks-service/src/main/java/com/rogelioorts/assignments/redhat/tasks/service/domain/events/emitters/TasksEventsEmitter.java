package com.rogelioorts.assignments.redhat.tasks.service.domain.events.emitters;

import com.rogelioorts.assignments.redhat.tasks.service.domain.models.Task;

public interface TasksEventsEmitter {

    void taskCreated(Task task);

}
