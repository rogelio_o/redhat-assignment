package com.rogelioorts.assignments.redhat.tasks.service.domain.services.impl;

import com.rogelioorts.assignments.redhat.tasks.service.domain.events.emitters.TasksEventsEmitter;
import com.rogelioorts.assignments.redhat.tasks.service.domain.models.Task;
import com.rogelioorts.assignments.redhat.tasks.service.domain.repositories.TasksRepository;
import com.rogelioorts.assignments.redhat.tasks.service.domain.services.TasksService;
import io.vertx.core.Future;
import lombok.RequiredArgsConstructor;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.List;

@RequiredArgsConstructor
public class TasksServiceImpl implements TasksService {

    private final TasksRepository tasksRepository;

    private final TasksEventsEmitter tasksEventsEmitter;

    private final Clock clock;

    @Override
    public Future<Task> createTask(final Task task) {
        task.setCreationDate(LocalDateTime.now(clock));

        return tasksRepository.createTask(task).map(createdTask -> {
            tasksEventsEmitter.taskCreated(createdTask);

            return createdTask;
        });
    }

    @Override
    public Future<List<Task>> getLastTasksOfUser(final int limit, final String username) {
        return tasksRepository.getLastTasksOfUser(limit, username);
    }

    @Override
    public Future<List<Task>> getLastTasksOfUserWithTag(final int limit, final String username, final String tag) {
        return tasksRepository.getLastTasksOfUserWithTag(limit, username, tag);
    }

}
