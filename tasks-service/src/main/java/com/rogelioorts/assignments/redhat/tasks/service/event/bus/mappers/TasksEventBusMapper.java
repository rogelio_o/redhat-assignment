package com.rogelioorts.assignments.redhat.tasks.service.event.bus.mappers;

import com.rogelioorts.assignments.redhat.tasks.service.domain.models.Task;
import com.rogelioorts.assignments.redhat.tasks.service.event.bus.models.TaskCreatedEvent;

public final class TasksEventBusMapper {

    private TasksEventBusMapper() {
    }

    public static TaskCreatedEvent taskCreatedEvent(final Task task) {
        final TaskCreatedEvent taskCreatedEvent = new TaskCreatedEvent();
        taskCreatedEvent.setId(task.getId());
        taskCreatedEvent.setUsername(task.getUsername());
        taskCreatedEvent.setTitle(task.getTitle());
        taskCreatedEvent.setDescription(task.getDescription());
        taskCreatedEvent.setTags(task.getTags());
        taskCreatedEvent.setDone(task.getDone());

        return taskCreatedEvent;
    }

}
