package com.rogelioorts.assignments.redhat.tasks.service.infrastructure.repositories;

import com.rogelioorts.assignments.redhat.tasks.service.domain.models.Task;
import com.rogelioorts.assignments.redhat.tasks.service.domain.repositories.TasksRepository;
import com.rogelioorts.assignments.redhat.tasks.service.infrastructure.mappers.TasksInfraMapper;
import com.rogelioorts.assignments.redhat.tasks.service.infrastructure.models.TaskDocument;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.FindOptions;
import io.vertx.ext.mongo.MongoClient;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class TasksMongoRepository implements TasksRepository {

    private static final String COLLECTION = "tasks";

    private final MongoClient client;

    @Override
    public Future<Task> createTask(final Task task) {
        final Future<Task> f = Future.future();
        final TaskDocument document = TasksInfraMapper.toInfra(task);

        client.save(COLLECTION, JsonObject.mapFrom(document), res -> {
            if (res.succeeded()) {
                document.setId(res.result());

                f.complete(TasksInfraMapper.toDomain(document));
            } else {
                f.fail(res.cause());
            }
        });

        return f;
    }

    @Override
    public Future<List<Task>> getLastTasksOfUser(final int limit, final String username) {
        final JsonObject query = new JsonObject()
            .put("username", username);

        return getLastTasksByQuery(limit, query);
    }

    @Override
    public Future<List<Task>> getLastTasksOfUserWithTag(final int limit, final String username, final String tag) {
        final JsonObject query = new JsonObject()
            .put("username", username)
            .put("tags", tag);

        return getLastTasksByQuery(limit, query);
    }

    private Future<List<Task>> getLastTasksByQuery(final int limit, final JsonObject query) {
        final Future f = Future.future();
        final FindOptions findOptions = new FindOptions()
            .setLimit(limit)
            .setSort(new JsonObject().put("creation_date", -1));

        client.findWithOptions(COLLECTION, query, findOptions, res -> {
            if (res.succeeded()) {
                final List<Task> tasks = res.result().stream()
                    .map(j -> j.mapTo(TaskDocument.class))
                    .map(TasksInfraMapper::toDomain)
                    .collect(Collectors.toList());

                f.complete(tasks);
            } else {
                f.fail(res.cause());
            }
        });

        return f;
    }
}
