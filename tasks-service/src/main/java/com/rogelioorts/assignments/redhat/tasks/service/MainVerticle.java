package com.rogelioorts.assignments.redhat.tasks.service;

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.rogelioorts.assignments.redhat.tasks.service.domain.events.emitters.TasksEventsEmitter;
import com.rogelioorts.assignments.redhat.tasks.service.domain.repositories.TasksRepository;
import com.rogelioorts.assignments.redhat.tasks.service.domain.services.TasksService;
import com.rogelioorts.assignments.redhat.tasks.service.domain.services.impl.TasksServiceImpl;
import com.rogelioorts.assignments.redhat.tasks.service.event.bus.emitters.TasksEventsEmitterEventBus;
import com.rogelioorts.assignments.redhat.tasks.service.infrastructure.repositories.TasksMongoRepository;
import com.rogelioorts.assignments.redhat.tasks.service.rest.config.JoseAuthHandler;
import com.rogelioorts.assignments.redhat.tasks.service.rest.config.JoseAuthProvider;
import com.rogelioorts.assignments.redhat.tasks.service.rest.controllers.TasksController;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.*;
import io.vertx.core.eventbus.EventBusOptions;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import lombok.extern.java.Log;

import java.time.Clock;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;

@Log
public class MainVerticle extends AbstractVerticle {

    private static final int PORT = 8083;

    private JsonObject config = new JsonObject();

    @Override
    public void start(final Future<Void> startFuture) {
        loadConfiguration(loadRes -> {
            try {
                loadBeans(beansRes -> {
                    if (beansRes.succeeded()) {
                        startWithBeans(beansRes.result(), startFuture);
                    } else {
                        beansRes.cause().printStackTrace();
                    }
                });
            } catch (Exception e) {
                log.log(Level.SEVERE, "Error loading beans.", e);
                startFuture.fail(e);
                vertx.close();
            }
        });
    }

    private void loadConfiguration(Handler<AsyncResult<Void>> handler) {
        Json.mapper.registerModule(new JavaTimeModule());

        ConfigStoreOptions env = new ConfigStoreOptions()
            .setType("env");

        ConfigRetrieverOptions options = new ConfigRetrieverOptions()
            .addStore(env);

        ConfigRetriever retriever = ConfigRetriever.create(vertx, options);
        retriever.getConfig(configRes -> {
            if (configRes.succeeded()) {
                config = configRes.result();

                handler.handle(Future.succeededFuture());
            } else {
                handler.handle(Future.failedFuture(configRes.cause()));
            }
        });
    }

    private void loadBeans(final Handler<AsyncResult<Map<String, Object>>> handler) throws Exception {
        final Map<String, Object> beans = new ConcurrentHashMap();

        final JsonObject mongoConfig = new JsonObject()
            .put("host", config.getString("MONGODB_HOST", "localhost"))
            .put("db_name", "tasks");
        final MongoClient mongoClient = MongoClient.createShared(vertx, mongoConfig);
        beans.put("MongoClient", mongoClient);

        final Clock clock = Clock.systemDefaultZone();
        beans.put("Clock", clock);

        final TasksRepository tasksRepository = new TasksMongoRepository(mongoClient);
        beans.put("TasksRepository", tasksRepository);

        final TasksEventsEmitter tasksEventsEmitter = new TasksEventsEmitterEventBus(vertx.eventBus());
        beans.put("TasksEventsEmitter", tasksEventsEmitter);

        final TasksService tasksService = new TasksServiceImpl(tasksRepository, tasksEventsEmitter, clock);
        beans.put("TasksService", tasksService);

        final TasksController tasksController = new TasksController(tasksService, vertx.eventBus());
        beans.put("TasksController", tasksController);

        final JoseAuthProvider joseAuthProvider = new JoseAuthProvider(JoseAuthProvider
            .createJWTConsumer(config.getString("JWT_VERIFIER_KEY_URI", "http://localhost:8081/tokens/jwt/key")));
        beans.put("JoseAuthProvider", joseAuthProvider);

        final JoseAuthHandler joseAuthHandler = new JoseAuthHandler(joseAuthProvider);
        beans.put("JoseAuthHandler", joseAuthHandler);

        handler.handle(Future.succeededFuture(beans));
    }

    private void startWithBeans(final Map<String, Object> beans, final Future<Void> startFuture) {
        createHttpServer(beans, startFuture);
    }

    private void createHttpServer(final Map<String, Object> beans, final Future<Void> startFuture) {
        final HttpServer server = vertx.createHttpServer();

        final Router router = createRouter(beans);

        final int port = getServerPort();
        server.requestHandler(router).listen(port, serverRes -> {
            log.info("Server started in " + port + ".");
            startFuture.complete();
        });
    }

    private Router createRouter(final Map<String, Object> beans) {
        final Router router = Router.router(vertx);
        final JoseAuthHandler joseAuthHandler = (JoseAuthHandler) beans.get("JoseAuthHandler");

        router.route().handler(BodyHandler.create());

        final TasksController tasksController = (TasksController) beans.get("TasksController");
        router
            .route(tasksController.getTasksMethod(), tasksController.getTasksPath())
            .handler(joseAuthHandler);
        router
            .route(tasksController.getTasksMethod(), tasksController.getTasksPath())
            .handler(tasksController::getTasks);
        router
            .route(tasksController.createTaskMethod(), tasksController.createTaskPath())
            .handler(joseAuthHandler);
        router
            .route(tasksController.createTaskMethod(), tasksController.createTaskPath())
            .handler(tasksController::createTaskMethod);
        router
            .route(tasksController.watchTasksMethod(), tasksController.watchTasksPath())
            .handler(joseAuthHandler);
        router
            .route(tasksController.watchTasksMethod(), tasksController.watchTasksPath())
            .handler(tasksController::watchTasks);

        return router;
    }

    private int getServerPort() {
        return config.getInteger("PORT", PORT);
    }

    public static void main(final String[] args) {
        final EventBusOptions eventBusOptions = new EventBusOptions();
        final String hostName = System.getenv("HOST_NAME");
        if (hostName != null) {
            eventBusOptions.setHost(hostName);
        }
        final VertxOptions options = new VertxOptions()
            .setEventBusOptions(eventBusOptions);
        Vertx.clusteredVertx(options, res -> {
            if (res.succeeded()) {
                Vertx vertx = res.result();
                vertx.deployVerticle(new MainVerticle());
            } else {
                log.log(Level.SEVERE, "Error getting clustered Vert.x", res.cause());
            }
        });
    }

}
