package com.rogelioorts.assignments.redhat.tasks.service.rest.controllers;

import com.rogelioorts.assignments.redhat.tasks.service.domain.models.Task;
import com.rogelioorts.assignments.redhat.tasks.service.domain.services.TasksService;
import com.rogelioorts.assignments.redhat.tasks.service.event.bus.models.TaskCreatedEvent;
import com.rogelioorts.assignments.redhat.tasks.service.rest.mappers.TasksRestMapper;
import com.rogelioorts.assignments.redhat.users.service.model.ApiListTasks;
import com.rogelioorts.assignments.redhat.users.service.model.ApiTask;
import com.rogelioorts.assignments.redhat.users.service.model.ApiTaskForm;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;

import java.util.List;

@RequiredArgsConstructor
@Log
public class TasksController {

    private static final int DEFAULT_NUM_TASKS = 50;

    private final TasksService tasksService;

    private final EventBus eventBus;

    public void getTasks(final RoutingContext context) {
        final String username = context.user().principal().getString("sub");
        final String tag = context.request().getParam("tag");

        final Future<List<Task>> future = tag == null ?
            tasksService.getLastTasksOfUser(DEFAULT_NUM_TASKS, username)
            : tasksService.getLastTasksOfUserWithTag(DEFAULT_NUM_TASKS, username, tag);
        future.setHandler(res -> {
            if (res.succeeded()) {
                final ApiListTasks tasks = TasksRestMapper.toRest(res.result());

                context.response()
                    .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                    .end(JsonObject.mapFrom(tasks).toBuffer());
            } else {
                context.fail(res.cause());
            }
        });
    }

    public String getTasksPath() {
        return "/";
    }

    public HttpMethod getTasksMethod() {
        return HttpMethod.GET;
    }

    public void createTaskMethod(final RoutingContext context) {
        final ApiTaskForm form = context.getBodyAsJson().mapTo(ApiTaskForm.class);
        final String username = context.user().principal().getString("sub");

        tasksService.createTask(TasksRestMapper.toDomain(form, username)).setHandler(res -> {
            if (res.succeeded()) {
                final ApiTask createdTask = TasksRestMapper.toRest(res.result());

                context.response()
                    .setStatusCode(201)
                    .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                    .end(JsonObject.mapFrom(createdTask).toBuffer());
            } else {
                context.fail(res.cause());
            }
        });
    }

    public String createTaskPath() {
        return "/";
    }

    public HttpMethod createTaskMethod() {
        return HttpMethod.POST;
    }

    public void watchTasks(final RoutingContext context) {
        final String username = context.user().principal().getString("sub");
        final HttpServerRequest request = context.request();
        final HttpServerResponse response = context.response();
        response.setChunked(true);

        final String accept = request.getHeader(HttpHeaders.ACCEPT);
        if (accept != null && !accept.contains("text/event-stream")) {
            context.fail(406);
            return;
        }

        final MessageConsumer<JsonObject> consumer = eventBus.consumer("task-created", msg -> {
            final TaskCreatedEvent event = msg.body().mapTo(TaskCreatedEvent.class);

            if (username.equals(event.getUsername())) {
                final ApiTask task = TasksRestMapper.toRest(event);

                response.write(JsonObject.mapFrom(task).toString() + "\n");
            }
        });
        response.closeHandler(v -> {
            consumer.unregister();
        });

        response
            .putHeader(HttpHeaders.CONTENT_TYPE, "text/event-stream")
            .putHeader(HttpHeaders.CACHE_CONTROL, "no-cache")
            .putHeader(HttpHeaders.CONNECTION, "keep-alive");
    }

    public String watchTasksPath() {
        return "/watch";
    }

    public HttpMethod watchTasksMethod() {
        return HttpMethod.GET;
    }

}
