package com.rogelioorts.assignments.redhat.tasks.service.event.bus.models;

import lombok.Data;

import java.util.List;

@Data
public class TaskCreatedEvent {

    private String id;

    private String username;

    private String title;

    private String description;

    private List<String> tags;

    private boolean done;

}
