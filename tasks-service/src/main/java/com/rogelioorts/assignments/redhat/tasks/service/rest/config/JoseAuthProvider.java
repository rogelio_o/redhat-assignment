package com.rogelioorts.assignments.redhat.tasks.service.rest.config;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.User;
import lombok.RequiredArgsConstructor;
import org.jose4j.jwk.HttpsJwks;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.keys.resolvers.JwksVerificationKeyResolver;
import org.jose4j.lang.JoseException;

import java.io.IOException;

@RequiredArgsConstructor
public class JoseAuthProvider implements AuthProvider {

    private final JwtConsumer jwtConsumer;

    @Override
    public void authenticate(final JsonObject jsonObject, final Handler<AsyncResult<User>> handler) {
        try {
            final JwtClaims claims = jwtConsumer.processToClaims(jsonObject.getString("jwt"));
            final JoseUser joseUser = new JoseUser(JsonObject.mapFrom(claims.getClaimsMap()));

            handler.handle(Future.succeededFuture(joseUser));
        } catch (InvalidJwtException e) {
            e.printStackTrace();
            handler.handle(Future.failedFuture(e));
        }
    }

    public static JwtConsumer createJWTConsumer(final String jwksENdpoint) throws JoseException, IOException {
        final HttpsJwks httpsJkws = new HttpsJwks(jwksENdpoint);
        final JwksVerificationKeyResolver jwksKeyResolver = new JwksVerificationKeyResolver(httpsJkws.getJsonWebKeys());
        return new JwtConsumerBuilder()
            .setVerificationKeyResolver(jwksKeyResolver)
            .build();
    }

}
