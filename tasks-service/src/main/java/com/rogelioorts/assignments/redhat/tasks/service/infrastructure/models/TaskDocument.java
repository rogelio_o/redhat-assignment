package com.rogelioorts.assignments.redhat.tasks.service.infrastructure.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TaskDocument {

    @JsonProperty("_id")
    private String id;

    private String username;

    private String title;

    private String description;

    private List<String> tags = new ArrayList<>();

    private Boolean done;

    @JsonProperty("creation_date")
    private LocalDateTime creationDate;

}
