package com.rogelioorts.assignments.redhat.tasks.service.infrastructure.mappers;

import com.rogelioorts.assignments.redhat.tasks.service.domain.models.Task;
import com.rogelioorts.assignments.redhat.tasks.service.infrastructure.models.TaskDocument;

public final class TasksInfraMapper {

    private TasksInfraMapper() {
    }

    public static Task toDomain(final TaskDocument infraTask) {
        final Task domainTask = new Task();
        domainTask.setId(infraTask.getId());
        domainTask.setUsername(infraTask.getUsername());
        domainTask.setTitle(infraTask.getTitle());
        domainTask.setDescription(infraTask.getDescription());
        domainTask.setTags(infraTask.getTags());
        domainTask.setDone(infraTask.getDone());
        domainTask.setCreationDate(infraTask.getCreationDate());

        return domainTask;
    }

    public static TaskDocument toInfra(final Task domainTask) {
        final TaskDocument infraTask = new TaskDocument();
        infraTask.setId(domainTask.getId());
        infraTask.setUsername(domainTask.getUsername());
        infraTask.setTitle(domainTask.getTitle());
        infraTask.setDescription(domainTask.getDescription());
        infraTask.setTags(domainTask.getTags());
        infraTask.setDone(domainTask.getDone());
        infraTask.setCreationDate(domainTask.getCreationDate());

        return infraTask;
    }

}
