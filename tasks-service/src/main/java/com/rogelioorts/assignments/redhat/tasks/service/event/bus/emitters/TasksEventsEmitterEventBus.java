package com.rogelioorts.assignments.redhat.tasks.service.event.bus.emitters;

import com.rogelioorts.assignments.redhat.tasks.service.domain.events.emitters.TasksEventsEmitter;
import com.rogelioorts.assignments.redhat.tasks.service.domain.models.Task;
import com.rogelioorts.assignments.redhat.tasks.service.event.bus.mappers.TasksEventBusMapper;
import com.rogelioorts.assignments.redhat.tasks.service.event.bus.models.TaskCreatedEvent;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class TasksEventsEmitterEventBus implements TasksEventsEmitter {

    private final EventBus eventBus;

    @Override
    public void taskCreated(Task task) {
        final TaskCreatedEvent taskCreatedEvent = TasksEventBusMapper.taskCreatedEvent(task);

        eventBus.publish(getEventId(), JsonObject.mapFrom(taskCreatedEvent));
    }

    public String getEventId() {
        return "task-created";
    }

}
