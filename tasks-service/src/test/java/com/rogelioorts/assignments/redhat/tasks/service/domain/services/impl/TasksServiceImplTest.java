package com.rogelioorts.assignments.redhat.tasks.service.domain.services.impl;

import com.rogelioorts.assignments.redhat.tasks.service.domain.events.emitters.TasksEventsEmitter;
import com.rogelioorts.assignments.redhat.tasks.service.domain.models.Task;
import com.rogelioorts.assignments.redhat.tasks.service.domain.repositories.TasksRepository;
import com.rogelioorts.assignments.redhat.tasks.service.domain.services.TasksService;
import io.vertx.core.Future;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(VertxUnitRunner.class)
public class TasksServiceImplTest {

    private TasksEventsEmitter tasksEventsEmitter;

    private Clock clock;

    @Before
    public void initializeTasksEventsEmitter() {
        tasksEventsEmitter = mock(TasksEventsEmitter.class);
    }

    @Before
    public void initializeClock() {
        final Instant now = Instant.now();
        clock = mock(Clock.class);
        when(clock.instant()).thenReturn(now);
        when(clock.getZone()).thenReturn(Clock.systemDefaultZone().getZone());
    }

    @Test
    public void checkAnEventIsEmittedAfterASuccessfulTaskCreation(final TestContext context) {
        final TasksService tasksService = createService(createSuccessfulRepository());

        final Async async = context.async();
        tasksService.createTask(createTask()).setHandler(res -> {
            if (res.succeeded()) {
                verify(tasksEventsEmitter, times(1)).taskCreated(res.result());

                async.complete();
            } else {
                context.fail(res.cause());
            }
        });
    }

    @Test
    public void checkAnEventIsNOTEmittedAfterAFailingTaskCreation(final TestContext context) {
        final TasksService tasksService = createService(createFailingRepository());

        final Async async = context.async();
        tasksService.createTask(createTask()).setHandler(res -> {
            if (res.succeeded()) {
                context.fail("NOT FAILING");
            } else {
                verify(tasksEventsEmitter, times(0)).taskCreated(res.result());

                async.complete();
            }
        });
    }

    @Test
    public void checkCurrentDateIsAddedBeforeTaskCreation(final TestContext context) {
        final TasksService tasksService = createService(createSuccessfulRepository());

        final Async async = context.async();
        tasksService.createTask(createTask()).setHandler(res -> {
            if (res.succeeded()) {
                context.assertEquals(LocalDateTime.now(clock), res.result().getCreationDate());

                async.complete();
            } else {
                context.fail(res.cause());
            }
        });
    }

    private TasksServiceImpl createService(final TasksRepository tasksRepository) {
        return new TasksServiceImpl(tasksRepository, tasksEventsEmitter, clock);
    }

    private Task createTask() {
        return new Task();
    }

    private TasksRepository createSuccessfulRepository() {
        final TasksRepository tasksRepository = mock(TasksRepository.class);
        when(tasksRepository.createTask(any(Task.class)))
            .then(answer -> Future.succeededFuture(answer.getArgument(0)));

        return tasksRepository;
    }

    private TasksRepository createFailingRepository() {
        final TasksRepository tasksRepository = mock(TasksRepository.class);
        when(tasksRepository.createTask(any(Task.class)))
            .thenReturn(Future.failedFuture("FAIL"));

        return tasksRepository;
    }

}
