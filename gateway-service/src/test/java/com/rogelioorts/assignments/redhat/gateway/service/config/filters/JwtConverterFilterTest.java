package com.rogelioorts.assignments.redhat.gateway.service.config.filters;

import com.rogelioorts.assignments.redhat.gateway.service.config.settings.GatewayHostsSettings;
import lombok.Getter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.http.server.reactive.MockServerHttpRequest;
import org.springframework.mock.web.server.MockServerWebExchange;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.when;

public class JwtConverterFilterTest {

    private static final String GATEWAY_HOST = "http://localhost:8080";
    private static final String AUTH_HOST = "http://localhost:8081";

    private static final String BEARER_TOKEN = "2a68adc0-e40e-457f-8f6e-785912177d07";
    private static final String JWT_TOKEN = "eyJraWQiOiJrMSIsImFsZyI6IlJTMjU2In0.eyJpc3MiOiJyZWRoYXQtYXNzaWdubWVudCIs" +
        "ImV4cCI6MTU1NTM1NzQ2NCwianRpIjoiOWlLLVhZT1QxY19FM2gwaGlyUGVwZyIsImlhdCI6MTU1NTE0MzkyNCwic3ViIjoidXNlciIsImN" +
        "saWVudF9pZCI6InRlc3QtY2xpZW50Iiwic2NvcGUiOlsiZGVsZXRlLXRhc2siLCJhZGQtdGFzayJdfQ.CNyPMGhmDkZY0-9tJO9zGLGxWXi" +
        "9Qm0MKt4PEqQj4wUlJRJr_qFX01aR0B0kCXNX6MPluAHZT12D7NXJTtTho8_jXtqoMQ1Zyz6lNEQRlz7f0MKXIhm4OAv1OYL5gTZJGVjnaB" +
        "JAhPU26qXV9SLYt_A5JbooV9gTol0fB4GL-TH_bZhHcmfRbEjGCXIg53YQLm6c8mU5bW-_afuZNAzQYzhp8tU3ja9lj3B65e_SYFmZPcn6X" +
        "-vn3tFGktBVcI6A42HmgUu9kWoNMux3ASZgc0F43OVXvFsXs9oMYX0XQ_sB1vtSCFxYR9LpeWsQ4YtiIrXp6n8Bqk5KbPy_DZBRpQ";

    private MockedGatewayFilterChain mockedChain;

    private GatewayHostsSettings gatewayHostsSettings;

    @Before
    public void initializeMockedChain() {
        mockedChain = new MockedGatewayFilterChain();
    }

    @Before
    public void initializeGatewayHostsSettings() {
        gatewayHostsSettings = new GatewayHostsSettings();
        gatewayHostsSettings.setAuthService(AUTH_HOST);
    }

    @Test
    public void testAuthorizationHeaderIsNotAddedIfNotExist() {
        final ServerWebExchange exchange = getWebExchangeWithoutToken();
        final JwtConverterFilter filter = new JwtConverterFilter(null, gatewayHostsSettings);

        filter.filter(exchange, mockedChain).block();

        Assert.assertNull(mockedChain.getHeaders().get(HttpHeaders.AUTHORIZATION));
    }

    @Test
    public void testAuthorizationHeaderIsReplacedByTheJWTTranslationIfExist() {
        final ServerWebExchange exchange = getWebExchangeWithToken();
        final JwtConverterFilter filter = new JwtConverterFilter(getWebClientMock(JWT_TOKEN), gatewayHostsSettings);

        filter.filter(exchange, mockedChain).block();

        Assert.assertEquals(
            Collections.singletonList(JWT_TOKEN),
            mockedChain.getHeaders().get(HttpHeaders.AUTHORIZATION));
    }

    @Test
    public void testAuthorizationHeaderIsCleanedUpIfTokenCannotBeConverted() {
        final ServerWebExchange exchange = getWebExchangeWithToken();
        final JwtConverterFilter filter = new JwtConverterFilter(
            getWebClientMockWithError(new RuntimeException("Not found")),
            gatewayHostsSettings);

        filter.filter(exchange, mockedChain).block();

        Assert.assertNull(mockedChain.getExchange().getRequest().getHeaders().get(HttpHeaders.AUTHORIZATION));
    }

    @Test
    public void testBearerPrefixIsRemovedWhenConvertionIsAsked() {
        final ServerWebExchange exchange = getWebExchangeWithToken();
        final ArgumentCaptor<String> tokenArgumentCaptor = ArgumentCaptor.forClass(String.class);
        final JwtConverterFilter filter = new JwtConverterFilter(
            getWebClientMock(JWT_TOKEN, tokenArgumentCaptor),
            gatewayHostsSettings);

        filter.filter(exchange, mockedChain).block();

        Assert.assertEquals(BEARER_TOKEN, tokenArgumentCaptor.getValue());
    }

    private ServerWebExchange getWebExchangeWithToken() {
        return MockServerWebExchange.from(MockServerHttpRequest.get(GATEWAY_HOST)
            .header(HttpHeaders.AUTHORIZATION, "Bearer " + BEARER_TOKEN));
    }

    private ServerWebExchange getWebExchangeWithoutToken() {
        return MockServerWebExchange.from(MockServerHttpRequest.get(GATEWAY_HOST));
    }

    private static WebClient getWebClientMock(final String resp) {
        return getWebClientMock(resp, ArgumentCaptor.forClass(String.class));
    }

    private static WebClient getWebClientMock(final String resp, final ArgumentCaptor<String> tokenArgumentCaptor) {
        final WebClient.ResponseSpec responseSpecMock = Mockito.mock(WebClient.ResponseSpec.class);

        when(responseSpecMock.bodyToMono(ArgumentMatchers.<Class<String>>notNull()))
            .thenReturn(Mono.just(resp));

        return getWebClientMockWithResponseSpec(tokenArgumentCaptor, responseSpecMock);
    }

    private static WebClient getWebClientMockWithError(Throwable throwable) {
        final WebClient.ResponseSpec responseSpecMock = Mockito.mock(WebClient.ResponseSpec.class);

        when(responseSpecMock.bodyToMono(ArgumentMatchers.<Class<String>>notNull()))
            .thenReturn(Mono.error(throwable));

        return getWebClientMockWithResponseSpec(ArgumentCaptor.forClass(String.class), responseSpecMock);
    }

    private static WebClient getWebClientMockWithResponseSpec(final ArgumentCaptor<String> tokenArgumentCaptor,
                                                              final WebClient.ResponseSpec responseSpecMock) {
        final WebClient mock = Mockito.mock(WebClient.class);
        final WebClient.RequestHeadersUriSpec uriSpecMock = Mockito.mock(WebClient.RequestHeadersUriSpec.class);
        final WebClient.RequestHeadersSpec headersSpecMock = Mockito.mock(WebClient.RequestHeadersSpec.class);

        when(mock.get()).thenReturn(uriSpecMock);
        when(uriSpecMock.uri(ArgumentMatchers.notNull(), tokenArgumentCaptor.capture())).thenReturn(headersSpecMock);
        when(headersSpecMock.header(notNull(), notNull())).thenReturn(headersSpecMock);
        when(headersSpecMock.headers(notNull())).thenReturn(headersSpecMock);
        when(headersSpecMock.retrieve()).thenReturn(responseSpecMock);

        return mock;
    }

    @Getter
    public class MockedGatewayFilterChain implements GatewayFilterChain {

        private ServerWebExchange exchange = null;

        @Override
        public Mono<Void> filter(ServerWebExchange exchange) {
            this.exchange = exchange;

            return Mono.empty();
        }

        public HttpHeaders getHeaders() {
            if (exchange == null) {
                return null;
            } else {
                return exchange.getRequest().getHeaders();
            }
        }
    }

}
