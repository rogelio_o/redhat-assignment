package com.rogelioorts.assignments.redhat.gateway.service.config;

import com.rogelioorts.assignments.redhat.gateway.service.config.filters.JwtConverterFilter;
import com.rogelioorts.assignments.redhat.gateway.service.config.settings.GatewayHostsSettings;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(GatewayHostsSettings.class)
@RequiredArgsConstructor
public class GatewayConfig {

    private final GatewayHostsSettings gatewayHostsSettings;

    private final JwtConverterFilter jwtConverterFilter;

    @Bean
    public RouteLocator customRouteLocator(final RouteLocatorBuilder builder) {
        return builder.routes()
            .route("auth", r -> r
                .path("/auth/oauth/**")
                .or().path("/auth/login")
                .or().path("/auth/logout")
                .filters(f -> f
                    .rewritePath("^/auth", ""))
                .uri(gatewayHostsSettings.getAuthService()))
            .route("tasks", r -> r.path("/tasks/**")
                .filters(f -> f
                    .rewritePath("^/tasks", "")
                    .filter(jwtConverterFilter))
                .uri(gatewayHostsSettings.getTasksService()))
            .route("users", r -> r.path("/users/current")
                .filters(f -> f
                    .rewritePath("^/users", "")
                    .filter(jwtConverterFilter))
                .uri(gatewayHostsSettings.getUsersService()))
            .build();
    }

}
