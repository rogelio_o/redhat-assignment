package com.rogelioorts.assignments.redhat.gateway.service.config.settings;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "gateway.hosts")
@Data
public class GatewayHostsSettings {

    private String authService;

    private String usersService;

    private String tasksService;

}
