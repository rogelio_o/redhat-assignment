package com.rogelioorts.assignments.redhat.gateway.service.config.filters;

import com.rogelioorts.assignments.redhat.gateway.service.config.settings.GatewayHostsSettings;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
@EnableConfigurationProperties(GatewayHostsSettings.class)
@RequiredArgsConstructor
public class JwtConverterFilter implements GatewayFilter {

    private final static String BEARER = "Bearer ";

    private final WebClient webClient;

    private final GatewayHostsSettings gatewayHostsSettings;

    @Override
    public Mono<Void> filter(final ServerWebExchange serverWebExchange, final GatewayFilterChain chain) {
        final ServerHttpRequest serverHttpRequest = serverWebExchange.getRequest();
        final List<String> authorizationHeader = serverHttpRequest.getHeaders().get(HttpHeaders.AUTHORIZATION);
        if (CollectionUtils.isEmpty(authorizationHeader)) {
            return chain.filter(serverWebExchange);
        } else {
            return convertToken(authorizationHeader.get(0)).flatMap(jwtToken -> {
                final ServerHttpRequest mutatedRequest = serverWebExchange.getRequest().mutate()
                    .headers((httpHeaders) -> {
                        if (StringUtils.isEmpty(jwtToken)) {
                            httpHeaders.remove(HttpHeaders.AUTHORIZATION);
                        } else {
                            httpHeaders.set(HttpHeaders.AUTHORIZATION, jwtToken);
                        }
                    })
                    .build();

                return chain.filter(serverWebExchange.mutate().request(mutatedRequest).build());
            });
        }
    }

    private Mono<String> convertToken(final String authorizationHeader) {
        final String bearerToken = authorizationHeader.startsWith(BEARER) ?
            authorizationHeader.substring(BEARER.length()) : authorizationHeader;

        return webClient.get()
            .uri(gatewayHostsSettings.getAuthService() + "/tokens/{bearerToken}/jwt", bearerToken)
            .retrieve().bodyToMono(String.class).onErrorReturn("");
    }

}
